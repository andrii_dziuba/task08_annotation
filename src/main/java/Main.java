import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Main main = Main.class.newInstance();
        for (Method declaredMethod : main.getClass().getDeclaredMethods()) {
            if (declaredMethod.isAnnotationPresent(DroidType.class)) {
                declaredMethod.invoke(main);
                DroidType annotation = declaredMethod.getDeclaredAnnotation(DroidType.class);
                System.out.println(declaredMethod + " [type: " + annotation.value() +
                        ", damage: " + annotation.damage() + ", health points: " + annotation.healthPoints() + "]");
            }
        }
        System.out.println();
        Method methodWithParams = main.getClass().getDeclaredMethod("methodWithParams", Object.class, int[].class);

        Object returned = methodWithParams.invoke(main,
                new StringBuilder().append(8).append(" custom").append(" words"),
                new int[]{0, 9, 8, 5});

        System.out.println("Returned object: " + returned);

    }

    @DroidType
    void battleDroid() {
        System.out.println("Method battleDroid invoked!");
    }

    @DroidType(value = "Droideka", damage = 50, healthPoints = 1000)
    void droideka() {
        System.out.println("Method droideka invoked!");
    }

    String methodWithParams(Object obj, int... ints) {
        System.out.println("methodWithParams: " + obj);
        return "♥♥♥" + obj.toString() + "♥♥♥";
    }
}



