import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface DroidType {
    String value() default "Battle droid";
    int damage() default 10;
    int healthPoints() default 100;
}